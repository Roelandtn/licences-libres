Quand on souhaite publier un projet et en faire profiter la communauté
(qu'elle soit civile ou scientifique) et permettre la réutilisation de ses
travaux, il est important de définir une licence suffisamment permissive.
Si l'auteur conserve ses droits sur son oeuvre, c'est à lui de définir 
ce qu'il autorise de faire avec (réemploie, modification, usage commercial, etc.).

Le présent guide condense des informations sur les licences libres, apporte
des éléments sur le logiciel libre et open source.

Il présente plusieurs démarches incitant les chercheurs et agents des services publics à publier leurs recherches (et si possible leur code) dans les chapitres "Démarche Science 
Ouverte" et "Démarche Public Money Public Code".

Des éléments d'aide au choix de la licence sont fournis, notamment le cadre
légal pour le code produit par des agents de la fonction publique.
Quelques exemples de projets IFSTTAR sont présentés.

Un chapitre propose un panorama des solutions disponibles pour la publication
de code source ainsi que les bonnes pratiques.

## A propos de ce document {-}

Ce document a été rédigé par Nicolas Roelandt, ingénieur d'études géomaticien
à l'[IFSTTAR](https://www.ifsttar.fr/accueil/) ([ORCID:0000-0001-9698-4275)](https://orcid.org/0000-0001-9698-4275).


```{r fig.align='center', echo=FALSE, warning=FALSE, message=FALSE}
knitr::include_graphics("images/IFSTTAR_COULEUR_FR.png")
```

Le présent document est en licence Creative Commons Attribution - Pas 
d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International. 
![CC-BY-SA icon](images/cc_by_sa_small.png)