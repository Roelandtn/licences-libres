# Démarches incitatives
## Démarche "Science Ouverte" (*OpenScience*)

Pour aller plus loin dans la démarche scientifique, de nombreux chercheurs
(tous domaines confondus) inscrivent leurs recherches dans une démarche "Science Ouverte".

En effet, pour que la science avance, les hypothèses formulées doivent être non seulement
éprouvées et testées par le chercheur mais aussi par des pairs. L'expérimentation
et ses résultats doivent pouvoir être reproductibles. C'est dans cette volonté
de permettre la reproductibilité et la transparence des expérimentations que c'est
développé le mouvement *Open Science*. Celui-ci repose sur plusieurs concepts :

* *open access*: 
  * accès ouvert à la publication
  * Licence ouverte au code ayant permis l'étude mais aussi la création des figures
* *relecture ouverte*
* *Données ouvertes*
* *méthode ouverte*

```{r openscience2, fig.align='center', fig.cap = "Science Ouverte [Wikipédia/Lamiot](https://commons.wikimedia.org/wiki/User:Lamiot?uselang=fr)", echo=FALSE, warning=FALSE, message=FALSE}

knitr::include_graphics("images/Science_ouverte_et_sujets_connexes.png")
```

L'accès à ces différents éléments permet aux autres chercheurs de tenter de reproduire
les expérimentations et d'en tirer leurs propres conclusions.

Le fait d'ouvrir le code qui a servi à produire les analyses, permet de détecter 
d'éventuelles erreurs mais font avancer la Science en proposant 

```{r reproductibility2, fig.align='center', fig.cap = "Définition de reproductibilité, [The Turing Way](https://the-turing-way.netlify.com/reproducibility/03/definitions.html)", echo=FALSE, warning=FALSE, message=FALSE}

knitr::include_graphics("images/ReproducibleMatrix.jpg")
```

Sur le schéma, il est possible de voir que pour être reproductible une étude doit
disposer des mêmes données et des mêmes outils d'analyse. Il est donc important de
libérer données et code.

Pour aller plus loin, le site 
[The Turing Way (en anglais)](https://the-turing-way.netlify.com/introduction/introduction.html)
détaille de nombreux aspects de la "Science Ouverte".

L'IFSTTAR s'est doté d'une [politique de diffusion en *open access* des publications 
](https://rapportactivite2018.ifsttar.fr/vie-scientifique/des-recherches-ouvertes-sur-la-societe/)
de ses chercheurs. Cela passe par la mise en place d'un [entrepôt de données ouvert institutionnel](https://research-data.ifsttar.fr/). Si vous souhaitez participer à cet effort d'ouverture, il est important
de bien choisir la licence à appliquer.

## Démarche *Public Money, Public Code*

Une autre initiative, lancée par le Free Software Foundation Europe, milite pour que le code source produit avec des fonds publics soit libéré.

> S’il s’agit d’argent public, le code devrait être également public

*source: [@europe_fsfe_public_2017]*

[publiccode.eu](https://publiccode.eu/fr/#about)


```{r pmpc2, fig.align='center', fig.cap = "Public Money, Public Code logo, [publiccode.eu](https://publiccode.eu/media/)", echo=FALSE, warning=FALSE, message=FALSE}

knitr::include_graphics("images/pmpc_logo_horizontal.jpg")
```


